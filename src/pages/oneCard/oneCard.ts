import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { OneCardService} from '../../services/oneCard-service';

@Component({
    selector: 'page-oneCard',
    templateUrl: 'oneCard.html'
})
export class OneCardPage {

    public card: { avatar: string, name: string, birth: string, image: string, message: string, heart: number, chat: number } = { avatar: '', name: '', birth: '', image: '', message: '', heart: 0, chat: 0 };
    public comments: any;
    constructor(public navCtrl: NavController, public oneCardService: OneCardService) {
        this.oneCardService.getCard(result => {
            this.card = result.card;
            this.comments = result.comments;
        });
    }
    
}
