import { Component } from '@angular/core';
import { NavController, ActionSheetController} from 'ionic-angular';
import { HomeService} from '../../services/home-service';
import { OneCardPage } from '../oneCard/oneCard';
import { NewPostPage } from '../newPost/newPost';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

    public cards: any;
    constructor(public navCtrl: NavController, public homeService: HomeService, public actionSheetCtrl: ActionSheetController) {
        this.homeService.getCards(result => {
            console.log(result);
            this.cards = result;
        });    
    }
    toogleHeart(card)
    {
        if (card.isHeart)
            card.heart--;
        else
            card.heart++;
        card.isHeart = !card.isHeart;
    }
    toogleChat(card) {
        if (card.isChat)
            card.chat--;
        else
            card.chat++;
        card.isChat = !card.isChat;
    }
     presentActionSheet() {
         let actionSheet = this.actionSheetCtrl.create({
             buttons: [
                 {
                     text: 'Delete',
                     role: 'destructive'
                 },
                 {
                     text: 'Share'
                 },
                 {
                     text: 'Favourite'                   
                 }, {
                     text: 'Cancel',
                     role: 'cancel'
                 }
             ]
         });
    actionSheet.present();
  }
     showCard() {
         this.navCtrl.push(OneCardPage);
     }
     newPost() {
         this.navCtrl.push(NewPostPage);
     }
}
