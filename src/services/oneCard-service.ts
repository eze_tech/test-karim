import {Injectable} from "@angular/core";

@Injectable()
export class OneCardService {
    constructor() { }

    getCard(callback) {
        var data = {
            card: { avatar: 'assets/img/adam.jpg', name: 'Ehab Ahmed', birth: '7th March 1991', image: 'assets/img/Jellyfish.jpg', message: 'Wait a minute. Wait a minute, Doc. Uhhh... Are you telling me that you', heart: 2, chat:5},
            comments: [{ avatar: 'assets/img/adam.jpg', name: 'Ehab Ahmed', time: '2 hours ago', heart: 2, isHeart: true, message: 'Are you telling me that you' },
                { avatar: 'assets/img/adam.jpg', name: 'Ehab Ahmed', time: '2 hours ago', heart: 4, isHeart: false, message: 'Are you telling me that you' },
                { avatar: 'assets/img/adam.jpg', name: 'Ehab Ahmed', time: '2 hours ago', heart: 7, isHeart: false, message: 'Are you telling me that you' },
                { avatar: 'assets/img/adam.jpg', name: 'Ehab Ahmed', time: '2 hours ago', heart: 2, isHeart: true, message: 'Are you telling me that you' }]
            }   
        callback(data);
    };
}