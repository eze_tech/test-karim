import {Injectable} from "@angular/core";

@Injectable()
export class HomeService {
    constructor() { }

    getCards(callback) {
        var data = [{ avatar: 'assets/img/adam.jpg', name: 'Ehab Ahmed', birth: '7th March 1991', image: 'assets/img/Jellyfish.jpg', message: 'Wait a minute. Wait a minute, Doc. Uhhh... Are you telling me that you', isHeart: false, heart: 5, isChat: true, chat: 8 },
            { avatar: 'assets/img/adam.jpg', name: 'Ehab Ahmed', birth: '7th March 1991', image: 'assets/img/Desert.jpg', message: 'Wait a minute. Wait a minute, Doc. Uhhh... Are you telling me that you', isHeart: true, heart: 7, isChat: true, chat: 3 },
            { avatar: 'assets/img/adam.jpg', name: 'Ehab Ahmed', birth: '7th March 1991', image: 'assets/img/Hydrangeas.jpg', message: 'Wait a minute. Wait a minute, Doc. Uhhh... Are you telling me that you', isHeart: true, heart: 2, isChat: false, chat: 6 },
            { avatar: 'assets/img/adam.jpg', name: 'Ehab Ahmed', birth: '7th March 1991', image: 'assets/img/Koala.jpg', message: 'Wait a minute. Wait a minute, Doc. Uhhh... Are you telling me that you', isHeart: false, heart: 1, isChat: true, chat: 4 }];
        callback(data);
    }
}