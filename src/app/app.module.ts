import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home'; 
import { OneCardPage } from '../pages/oneCard/oneCard';
import { NewPostPage } from '../pages/newPost/newPost';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomeService} from '../services/home-service';
import { OneCardService} from '../services/oneCard-service';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    OneCardPage,
    NewPostPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    OneCardPage,
    NewPostPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HomeService,
    OneCardService
  ]
})
export class AppModule {}
