import { Component, ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';


@Component({
  templateUrl: 'app.html',
   queries: {
    nav: new ViewChild('content')
  }
})
export class MyApp {
    public nav: any;
    public rootPage: any;
    public pages = [
        {
            title: 'Home',
            icon: 'ios-home-outline',
            count: 0
        },
        {
            title: 'Post',
            icon: 'ios-paper-outline',
            count: 0
        },
        {
            title: 'Chats',
            icon: 'ios-mail-outline',
            count: 2
        },
        {
            title: 'Notifications',
            icon: 'ios-notifications-outline',
            count: 5
        },
        {
            title: 'Contact',
            icon: 'ios-person-outline',
            count: 0
        },
        {
            title: 'Setting',
            icon: 'ios-settings-outline',
            count: 0
        },
        {
            title: 'Logout',
            icon: 'ios-log-out-outline',
            count: 0
        }
    ];
  
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
      platform.ready().then(() => {
          this.nav.setRoot(HomePage);
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
